<?php

use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

require_once "setup.php";

$app->get('/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        //TODO: Add loging message
        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'website/login.html.twig');
});

$app->post('/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $loginInfo = $request->getParsedBody();

    if ($loginInfo != null) {
        if (isset($loginInfo['email']) && isset($loginInfo['password'])) {
            $user = DB::queryFirstRow("SELECT * FROM users WHERE email= :s", $loginInfo['email']);
            if ($loginInfo['password'] === $user['password']) {
                unset($user['password']);
                $_SESSION['user'] = $user;
                return $response
                    ->withHeader('Location', '/');
            }
        }
    }

    return $view->render($response, 'website/login.html.twig', [
        'error' => "Email doesn't match password."
    ]);
});

$app->get('/logout', function (Request $request, Response $response) {
    unset($_SESSION['user']);
    return $response->withHeader('Location', '/');
});

// returns error message or TRUE if password is okay
function verifyPasswordQuality($password)
{
    if (
        strlen($password) < 6 || strlen($password) > 100
        || preg_match("/[a-z]/", $password) == false
        || preg_match("/[A-Z]/", $password) == false
        || preg_match("/[0-9#$%^&*()+=-\[\]';,.\/{}|:<>?~]/", $password) == false
    ) {
        return "Password must be 6~100 characters,
                            must contain at least one uppercase letter, 
                            one lower case letter, 
                            and one number or special character.";
    }
    return TRUE;
}

$app->get('/register', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'website/register.html.twig');
});

$app->post('/register', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $registerInfo = $request->getParsedBody();
    $email = $registerInfo['email'];
    $name = $registerInfo['name'];
    $password = $registerInfo['password'];
    $confirmPassword = $registerInfo['confirmPassword'];
    $phone = $registerInfo['phone'];
    $postcode = $registerInfo['postcode'];
    $address = $registerInfo['address'];
    $city = $registerInfo['city'];
    $province = $registerInfo['province'];
    $country = $registerInfo['country'];
    $errors = [];

    if (strlen($name) < 5 || strlen($name) > 20) {
        $errors['name'] = "Name must be 5~20 chars";
        $registerInfo['name'] = '';
    }

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $errors['email'] = "Invalid Email";
        $email = '';
    } elseif (isEmailTaken($email)) {
        $errors['email'] = "User is already exist.";
        $email = '';
    }

    if (strlen($phone) < 6 || strlen($phone) > 24) {
        $errors['phone'] = "phone must be 6~24 chars";
        $registerInfo['phone'] = '';
    }

    if (strlen($postcode) < 3 || strlen($postcode) > 10) {
        $errors['postcode'] = "postcode must be 3~10 chars";
        $registerInfo['phone'] = '';
    }

    if (strlen($address) < 5 || strlen($address) > 60) {
        $errors['address'] = "address must be 5~60 chars";
        $registerInfo['address'] = '';
    }

    if (strlen($city) < 1 || strlen($city) > 15) {
        $errors['city'] = "city must be 5~15 chars";
        $registerInfo['address'] = '';
    }

    if (strlen($province) < 1 || strlen($province) > 10) {
        $errors['province'] = "province must be 5~60 chars";
        $registerInfo['province'] = '';
    }
    if (strlen($country) < 1 || strlen($country) > 15) {
        $errors['country'] = "country must be 1~15 chars";
        $registerInfo['country'] = '';
    }

    $passQuality = verifyPasswordQuality($password);
    if ($passQuality !== TRUE) {
        $errors['password'] = $passQuality;
    } elseif ($password !== $_POST['confirmPassword']) {
        $errors['password'] = "Passwords must be same.";
    }

    if (empty($errors)) {
        DB::insert('users', [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'phone' => $phone,
            'postcode' => $postcode,
            'address' => $address,
            'city' => $city,
            'province' => $province,
            'country' => $country,
        ]);
        $_SESSION['users'] = DB::queryFirstRow("SELECT * FROM users WHERE email = :s", $email);

        return $view->render($response, 'website/login.html.twig', ['resgisterSuccess' => true, 'email' => $email]);
    }

    return $view->render($response, 'website/register.html.twig', [
        'errors' => $errors,
        'prevInput' => [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'postcode' => $postcode,
            'address' => $address,
            'city' => $city,
            'province' => $province,
            'country' => $country,
        ]
    ]);
});

$app->get('/deliveryregister', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'website/deliveryregister.html.twig');
});

$app->post('/deliveryregister', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $registerInfo = $request->getParsedBody();
    $email = $registerInfo['email'];
    $password = $registerInfo['password'];
    $confirmPassword = $registerInfo['confirmPassword'];    
    $name = $registerInfo['name'];
    $dateOfBirthday = $registerInfo['dateOfBirthday'];
    $workType = $registerInfo['workType'];
    $phone = $registerInfo['phone'];
    $driverFrom = $registerInfo['driverFrom'];
    $driverLicense = $registerInfo['driverLicense'];
    $postcode = $registerInfo['postcode'];
    $address = $registerInfo['address'];
    $city = $registerInfo['city'];
    $province = $registerInfo['province'];
    $country = $registerInfo['country'];
    $errors = [];

    if (strlen($name) < 5 || strlen($name) > 20) {
        $errors['name'] = "name must be 5~20 chars";
        $registerInfo['name'] = '';
    }

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $errors['email'] = "Invalid Email";
        $email = '';
    } elseif (isEmailTaken($email)) {
        $errors['email'] = "User is already exist.";
        $email = '';
    }

    if (strlen($driverLicense) < 5 || strlen($driverLicense) > 20) {
        $errors['driverLicense'] = "driverLicense must be 5~20 chars";
        $registerInfo['driverLicense'] = '';
    }

    if (strlen($phone) < 6 || strlen($phone) > 24) {
        $errors['phone'] = "phone must be 6~24 chars";
        $registerInfo['phone'] = '';
    }

    if (strlen($postcode) < 3 || strlen($postcode) > 10) {
        $errors['postcode'] = "postcode must be 3~10 chars";
        $registerInfo['phone'] = '';
    }

    if (strlen($address) < 5 || strlen($address) > 60) {
        $errors['address'] = "address must be 5~60 chars";
        $registerInfo['address'] = '';
    }

    if (strlen($city) < 1 || strlen($city) > 15) {
        $errors['city'] = "city must be 5~15 chars";
        $registerInfo['address'] = '';
    }

    if (strlen($province) < 1 || strlen($province) > 10) {
        $errors['province'] = "province must be 5~60 chars";
        $registerInfo['province'] = '';
    }
    if (strlen($country) < 1 || strlen($country) > 15) {
        $errors['country'] = "country must be 1~15 chars";
        $registerInfo['country'] = '';
    }

    $passQuality = verifyPasswordQuality($password);
    if ($passQuality !== TRUE) {
        $errors['password'] = $passQuality;
    } elseif ($password !== $_POST['confirmPassword']) {
        $errors['password'] = "Passwords must be same.";
    }    

    if (empty($errors)) {
        DB::insert('deliverymen', [
            'name' => $name,
            'email' => $email,
            'password' => $password,            
            'workType' => $workType,
            'dateOfBirthday' => $dateOfBirthday,
            'driverFrom' => $driverFrom,
            'driverLicense' => $driverLicense,
            'phone' => $phone,
            'postcode' => $postcode,
            'address' => $address,
            'city' => $city,
            'province' => $province,
            'country' => $country,
        ]);
        return $view->render($response, 'website/drivverregistersuccess.html.twig');
    }

    return $view->render($response, 'website/deliveryregister.html.twig', [
        'errors' => $errors,
        'prevInput' => [
            'name' => $name,
            'email' => $email,
            'workType' => $workType,
            'dateOfBirthday' => $dateOfBirthday,
            'driverFrom' => $driverFrom,
            'driverLicense' => $driverLicense,
            'phone' => $phone,
            'postcode' => $postcode,
            'address' => $address,
            'city' => $city,
            'province' => $province,
            'country' => $country,
        ]
    ]);
});

$app->get('/restaurantregister', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'website/restaurantregister.html.twig');
});

$app->post('/restaurantregister', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $registerInfo = $request->getParsedBody();
    $email = $registerInfo['email'];
    $name = $registerInfo['name'];
    $owner = $registerInfo['owner'];    
    $phone = $registerInfo['phone'];
    $postcode = $registerInfo['postcode'];
    $address = $registerInfo['address'];
    $city = $registerInfo['city'];
    $province = $registerInfo['province'];
    $country = $registerInfo['country'];
    $errors = [];

    if (strlen($name) < 5 || strlen($name) > 20) {
        $errors['name'] = "name must be 5~20 chars";
        $registerInfo['name'] = '';
    }

    if (strlen($owner) < 5 || strlen($owner) > 20) {
        $errors['owner'] = "owner must be 5~20 chars";
        $registerInfo['owner'] = '';
    }    

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $errors['email'] = "Invalid Email";
        $email = '';
    } elseif (isEmailTaken($email)) {
        $errors['email'] = "User is already exist.";
        $email = '';
    }

    if (strlen($phone) < 6 || strlen($phone) > 24) {
        $errors['phone'] = "phone must be 6~24 chars";
        $registerInfo['phone'] = '';
    }

    if (strlen($postcode) < 3 || strlen($postcode) > 10) {
        $errors['postcode'] = "postcode must be 3~10 chars";
        $registerInfo['phone'] = '';
    }

    if (strlen($address) < 5 || strlen($address) > 60) {
        $errors['address'] = "address must be 5~60 chars";
        $registerInfo['address'] = '';
    }

    if (strlen($city) < 1 || strlen($city) > 15) {
        $errors['city'] = "city must be 5~15 chars";
        $registerInfo['address'] = '';
    }

    if (strlen($province) < 1 || strlen($province) > 10) {
        $errors['province'] = "province must be 5~60 chars";
        $registerInfo['province'] = '';
    }
    if (strlen($country) < 1 || strlen($country) > 15) {
        $errors['country'] = "country must be 1~15 chars";
        $registerInfo['country'] = '';
    }

    if (empty($errors)) {
        DB::insert('restaurants', [
            'name' => $name,
            'owner' => $owner,
            'email' => $email,        
            'phone' => $phone,
            'postcode' => $postcode,
            'address' => $address,
            'city' => $city,
            'province' => $province,
            'country' => $country,
        ]);
        return $view->render($response, 'website/restaurantregistersuccess.html.twig');
    }

    return $view->render($response, 'website/restaurantregister.html.twig', [
        'errors' => $errors,
        'prevInput' => [
        'name' => $name,
        'owner' => $owner,
        'email' => $email,
        'phone' => $phone,
        'postcode' => $postcode,
        'address' => $address,
        'city' => $city,
        'province' => $province,
        'country' => $country,
        ]
    ]);
});


$app->get('/register/isemailtaken/[{email}]', function (Request $request, Response $response, array $args) {
    $error = '';

    if (isset($args['email'])) {
        $error = isEmailTaken($args['email']) ? "Email It's already taken." : '';
    }

    $response->getBody()->write($error);
    return $response;
});


function isEmailTaken($email)
{
    $users = DB::queryFirstRow("SELECT COUNT(*) AS 'count' FROM users WHERE email = :s", $email);
    if ($users['count'] == 0) {
        return false;
    } elseif ($users['count'] == 1) {
        return true;
    } else {
        echo "Internal Error: duplicate name."; //FIXME : Log instead of echoing
        return true;
    }
}
