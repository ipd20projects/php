<?php

use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

require_once "setup.php";

$app->get('/admin/', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $restaurantsList = DB::query("SELECT * FROM restaurants ");
    return $view->render($response, 'admin/restaurants.html.twig', ['restaurantsList' => $restaurantsList]);
});

$app->get('/admin/restaurants', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $restaurantsList = DB::query("SELECT * FROM restaurants ");
    return $view->render($response, 'admin/restaurants.html.twig', ['restaurantsList' => $restaurantsList]);
});
$app->get('/admin/users', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $users = DB::query("SELECT * FROM users");
    return $view->render($response, 'admin/users.html.twig', ['users' => $users]);
});

$app->get('/admin/deliverymen', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $deliverymen = DB::query("SELECT * FROM deliverymen");
    return $view->render($response, 'admin/deliverymen.html.twig', ['deliverymen' => $deliverymen]);
});

$app->get('/admin/categories', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $restaurants = DB::query("SELECT * FROM restaurants ");
    $categories = DB::query("SELECT * FROM categories");
    return $view->render(
        $response,
        'admin/categories.html.twig',
        ['categories' => $categories, 'restaurants' => $restaurants]
    );
});

$app->get('/admin/menus', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $menus = DB::query("SELECT M.id AS id,M.name AS name,M.inventory AS inventory, 
                        M.price AS price,M.pictureFilePath AS picture,C.name AS categoryName,
                        R.name AS restaurantName

                        FROM menus AS M
                        INNER JOIN categories AS C
                            ON M.categoryId = C.id
                        INNER JOIN restaurants AS R
                            ON C.restaurantId = R.id
                        ");
    return $view->render(
        $response,
        'admin/menus.html.twig',
        ['menus' => $menus]
    );
});

$app->get('/admin/orders', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $orders = DB::query("SELECT O.*,U.name AS customerName,R.name AS restaurantName,D.name AS deliveryMan
                        FROM orders AS O
                        LEFT JOIN deliverymen AS D
                            ON O.deliveryManId = D.id                            
                        INNER JOIN users AS U
                            ON O.customerId = U.id
                        INNER JOIN restaurants AS R
                            ON O.restaurantId = R.id                       
                        ");
    return $view->render(
        $response,
        'admin/orders.html.twig',
        ['orders' => $orders]
    );
});

$app->get('/admin/deliveryhistory', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $orders = DB::query("SELECT O.*,R.name AS restaurantName,R.shippingFee AS shippingFee,
                        U.name AS customerName,D.name AS deliveryMan
                        FROM orders AS O
                        INNER JOIN deliverymen AS D
                            ON O.deliveryManId = D.id
                        INNER JOIN restaurants AS R
                            ON O.restaurantId = R.id
                        INNER JOIN users AS U
                            ON O.customerId = U.id
                        WHERE O.status='deliveried'");
    return $view->render(
        $response,
        'admin/deliveryhistory.html.twig',
        ['orders' => $orders]
    );
});

$app->get('/admin/changepassword', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'admin/changepassword.html.twig');
});

$app->post('/admin/changepassword', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $registerInfo = $request->getParsedBody();

    $password = $registerInfo['password'];

    $confirmPassword = $registerInfo['confirmPassword'];
    $passQuality = verifyPasswordQuality($password);
    if ($passQuality !== TRUE) {
        $errors['password'] = $passQuality;
    } elseif ($password !== $_POST['confirmPassword']) {
        $errors['password'] = "Passwords must be same.";
    }
    if (empty($errors)) {
        DB::query( "update employees set password=:s where id=:s",$password, $_SESSION['userId'] );
        return $view->render($response, 'admin/changepassword.html.twig', ['changeSuccess' => true] );
    }

    return $view->render($response, 'admin/changepassword.html.twig', [
        'errors' => $errors,
    ]);
});
