<?php
use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Slim\Http\UploadedFile;

session_start();

require __DIR__ . '/../vendor/autoload.php';

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('../logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('../logs/errors.log', Logger::ERROR));

require_once '../setup/setupdb.php';

$container = new Container();

AppFactory::setContainer($container);
$app = AppFactory::create();

// Create Twig
//$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache','debug'=>true]);
$twig = Twig::create(__DIR__ . '/templates', ['debug'=>true]);
//$twig->addGlobal('session', $_SESSION);
//$twig->getEnvironment()->addGlobal('session', isset($_SESSION['user']) ? $_SESSION['user'] : false);

$twig->getEnvironment()->addGlobal('session', $_SESSION);

$app->addErrorMiddleware(true,true,true);
// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));
