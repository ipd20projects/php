<?php
use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;
// PHPmailer
//use PHPMailer\PHPMailer\PHPMailer;
//require "PHPMailer/PHPMailer.php";
//require "PHPMailer/Exception.php";
    
require_once "setup.php";
// return item count in cart
$app->get('/api/cart', function (Request $request, Response $response, array $args) {
    $itemCount = DB::queryFirstField("SELECT SUM(quantity) AS itemCount FROM cartitems WHERE sessionId=%s", session_id());
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    
    $response= $response->withStatus(201);// record created
    $response->getBody()->write(json_encode($itemCount));
    return $response;
});
$app->post('/api/addtocart', function (Request $request, Response $response, array $args) {
    
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $json = $request->getBody();
    $cartitem = json_decode($json,true); // true makes it return an associative array instead of an object
    $productId = $cartitem['productId'];
    $quantity = $cartitem['quantity'];
    $sellerId = DB::queryFirstField("SELECT sellerId FROM products WHERE id=%i", $productId);
    $sessionId = session_id();
    // validate cartitem fields
    if (($result = validateCartitem($cartitem)) !== TRUE){
        global $log;
        $log->debug("POST /api/addtocart failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    $item = DB::queryFirstRow("SELECT * FROM cartitems WHERE productId=%i AND sessionId=%s", $productId, $sessionId);
    //print_r($item);
    if ($item) {
        $quantity += $item['quantity'];
        DB::update('cartitems', array(
            'sessionId' => session_id(),
            'productId' => $productId,
            'sellerId' => $sellerId,
            'quantity' =>  $quantity
                ), "productId=%d AND sessionId=%s", $productId, session_id());
    } else {
        $cartitem['sessionId'] = $sessionId;
        $cartitem['sellerId'] = $sellerId;
        DB::insert('cartitems', $cartitem);
    }
    $id = DB::insertId();
    $itemCount = DB::queryFirstField("SELECT SUM(quantity) AS itemCount FROM cartitems WHERE sessionId=%s", $sessionId);
    $response= $response->withStatus(201);// record created
    $response->getBody()->write(json_encode($itemCount));
    return $response;
});

function isInteger($input){
    return(ctype_digit(strval($input)));
}
// return TRUE if all is fine otherwise return string describing the problem
function validateCartitem($cartitem) {
    
    if ($cartitem == NULL) { // if json_decode fails it returns null - handle it here
        return "Invalid data provided";
    }
    foreach($cartitem as $key=>$value){
        if ($value==null){
            return $key . " shouldn't be null";
        }
    }
    // does it have all the fields required and only the fields required?
    $expectedFields = ['productId', 'quantity'];
    $cartitemFields = array_keys($cartitem);
    // check for fields that should not be there
    if (($diff = array_diff($cartitemFields, $expectedFields))) {
        return "Invalid field in Cartitem: [" . implode(',',$diff) . "]";
    }
    // check for fields that are missing
    if (($diff = array_diff($expectedFields,$cartitemFields))) {
        return "Missing field in cartitem: [" . implode(',',$diff) . "]";
    }

    // validate each field
    if (count(DB::queryFirstRow("SELECT * FROM products WHERE id=%i",$cartitem['productId']))==0) {
        return "product doesn't exist";
    }
    if ($cartitem['quantity']<1 || (isInteger($cartitem['quantity'])===FALSE)){
        return "invalid quantity". $cartitem['quantity'];
    }
    
    return TRUE;

}

$app->get('/cart', function(Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    // TODO: retrieve addProdId and quantity from URL *IF* present
    $sellerList = DB::query(
        "SELECT DISTINCT users.username as name, users.id"
        . " FROM cartitems, users "
        . " WHERE cartitems.sellerId=users.id AND sessionID=%s", session_id());
    
    $cartitemList = DB::query(
                    "SELECT cartitems.id  as id, cartitems.sellerId, productId, quantity,"
                    . " name, description, picture, unitPrice"
                    . " FROM cartitems, products "
                    . " WHERE cartitems.productId = products.id AND sessionID=%s", session_id());
    foreach($cartitemList as &$ci){
        $ci['picture'] = base64_encode($ci['picture']);
    }
                    return $view->render($response, 'cart.html.twig', ['cartitemList' => $cartitemList, 'sellerList' => $sellerList]);
    
});

// AJAX call, not used directy by user
$app->patch('/cart/update/{cartitemId:[0-9]+}/{quantity:[0-9]+}', function(Request $request, Response $response, array $args){
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $cartitemId = $args['cartitemId'];
    $quantity = $args['quantity'];

    if ($quantity == 0) {
        DB::delete('cartitems', 'cartitems.id=%d AND cartitems.sessionId=%s', $cartitemId, session_id());
    } else {
        DB::update('cartitems', ['quantity' => $quantity], 'cartitems.id=%d AND cartitems.sessionId=%s', $cartitemId, session_id());
    }
    $response= $response->withStatus(201);// record created
    $response->getBody()->write(json_encode(DB::affectedRows() == 1));
    return $response;
});
$app->patch('/cart/status/{cartitemId:[0-9]+}/{status:[0-1]}', function(Request $request, Response $response, array $args){
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $cartitemId = $args['cartitemId'];
    $status = $args['status'];

    DB::update('cartitems', ['status' => $status], 'cartitems.id=%d AND cartitems.sessionId=%s', $cartitemId, session_id());
    $response= $response->withStatus(201);// record created
    $response->getBody()->write(json_encode(DB::affectedRows() == 1));
    return $response;
});
$app->patch('/cart/statusstore/{sellerId:[0-9]+}/{status:[0-1]}', function(Request $request, Response $response, array $args){
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $sellerId = $args['sellerId'];
    $status = $args['status'];

    DB::update('cartitems', ['status' => $status], 'cartitems.sellerId=%d AND cartitems.sessionId=%s', $sellerId, session_id());
    $response= $response->withStatus(201);// record created
    $response->getBody()->write(json_encode(DB::affectedRows() == 1));
    return $response;
});
$app->patch('/cart/statusall/{status:[0-1]}', function(Request $request, Response $response, array $args){
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $status = $args['status'];

    DB::update('cartitems', ['status' => $status], 'cartitems.sessionId=%s', session_id());
    $response= $response->withStatus(201);// record created
    $response->getBody()->write(json_encode(DB::affectedRows() == 1));
    return $response;
});
// order handling
$app->map(['GET','POST'],'/order', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $totalBeforeTax = DB::queryFirstField(
                    "SELECT SUM(products.unitPrice * cartitems.quantity) "
                    . " FROM cartitems, products "
                    . " WHERE cartitems.sessionID=%s AND cartitems.productID=products.ID AND cartitems.status=1", session_id());
    // TODO: properly compute taxes, shipping, ...
    $shippingBeforeTax = 15.00;
    $taxes = ($totalBeforeTax + $shippingBeforeTax) * 0.15;
    $totalWithShippingAndTaxes = $totalBeforeTax + $shippingBeforeTax + $taxes;
    $cartitemList = DB::query(
        "SELECT cartitems.id  as id, cartitems.sellerId, productId, quantity,"
        . " name, description, picture, unitPrice"
        . " FROM cartitems, products "
        . " WHERE cartitems.sessionID=%s AND cartitems.productID=products.ID AND cartitems.status=1", session_id());
    $sellerList = DB::query(
            "SELECT DISTINCT users.username as name, users.id"
            . " FROM cartitems, users "
            . " WHERE cartitems.sellerId=users.id AND cartitems.status=1 AND sessionID=%s", session_id());
    if ($request->getMethod() == "GET") {
        return $view->render($response,'order.html.twig', [
            'totalBeforeTax' => number_format($totalBeforeTax, 2),
            'shippingBeforeTax' => number_format($shippingBeforeTax, 2),
            'taxes' => number_format($taxes, 2),
            'totalWithShippingAndTaxes' => number_format($totalWithShippingAndTaxes, 2),
            'cartitemList' => $cartitemList,
            'sellerList' => $sellerList
        ]);
    } else { // POST - order form submitted
        $postVars = $request->getParsedBody();
        $name = $postVars['name'];
        $email = $postVars['email'];
        $address = $postVars['address'];
        $postalCode = $postVars['postalCode'];
        $phoneNumber = $postVars['phoneNumber'];
        $valueList = array(
            'name' => $name,
            'email' => $email,
            'address' => $address,
            'postalCode' => $postalCode,
            'phoneNumber' => $phoneNumber
        );
        // FIXME: verify inputs - MUST DO IT IN A REAL SYSTEM
        $errorList = array();
        //
        if ($errorList) {
            return $view->render($response,'order.html.twig', array(
                'totalBeforeTax' => number_format($totalBeforeTax, 2),
                'shippingBeforeTax' => number_format($shippingBeforeTax, 2),
                'taxes' => number_format($taxes, 2),
                'totalWithShippingAndTaxes' => number_format($totalWithShippingAndTaxes, 2),
                'v' => $valueList
            ));
        } else { // SUCCESSFUL SUBMISSION
            DB::$error_handler = FALSE;
            DB::$throw_exception_on_error = TRUE;
            //TODO: validate fields, if buyer hasn't logged in, grayout the login form
            $buyerId = $_SESSION['user'] ? $_SESSION['user']['id']: NULL;
            if (!$buyerId){
                return $view->render($response,'login.html.twig');
            }
            // PLACE THE ORDER
            try {
                DB::startTransaction();
                // 1. create summary record in 'orders' table (insert)
                //id	buyerId	sellerId	orderTime	total	orderStatus	shippingAddress	postalcode	recieverName	phoneNo
                $sellerList = DB::query(
                    "SELECT DISTINCT users.username as name, users.id"
                    . " FROM cartitems, users "
                    . " WHERE cartitems.sellerId=users.id AND cartitems.status=1 AND sessionID=%s", session_id());
                    
                foreach($sellerList as $seller){
                    $sellerId = $seller['id'];
                    $totalBeforeTax = DB::queryFirstField(
                        "SELECT SUM(products.unitPrice * cartitems.quantity) "
                        . " FROM cartitems, products "
                        . " WHERE cartitems.sessionID=%s AND cartitems.sellerId=%i AND cartitems.productID=products.ID AND cartitems.status=1", session_id(),$sellerId);
                    // TODO: properly compute taxes, shipping, ...
                    $shippingBeforeTax = 15.00;
                    $taxes = ($totalBeforeTax + $shippingBeforeTax) * 0.15;
                    $totalWithShippingAndTaxes = $totalBeforeTax + $shippingBeforeTax + $taxes;
                    DB::insert('orders', [
                        'buyerId' => $buyerId,
                        'sellerId' => $sellerId,
                        'recieverName' => $name,
                        'shippingAddress' => $address,
                        'postalcode' => $postalCode,
                        'email' => $email,
                        'phoneNo' => $phoneNumber,
                        'totalBeforeTax' => $totalBeforeTax,
                        'shippingBeforeTax' => $shippingBeforeTax,
                        'taxes' => $taxes,
                        'total' => $totalWithShippingAndTaxes,
                        'orderStatus' => 'placed'
                    ]);
                    $orderId = DB::insertId();
                    // 2. copy all records from cartitems to 'orderitems' (select & insert)
                    //id	orderId	productId	unitPrice	buyPrice	buyCount	subtotal
                    $cartitemList = DB::query(
                    "SELECT DISTINCT productId, quantity as buyCount, unitPrice"
                    . " FROM cartitems, products "
                    . " WHERE cartitems.productId = products.id AND cartitems.status=1 AND cartitems.sellerId=%i AND sessionId=%s", $sellerId, session_id());
                    // add orderId to every sub-array (element) in $cartitemList
                    array_walk($cartitemList, function(&$item, $key) use ($orderId) {
                        $item['orderId'] = $orderId;
                        $item['buyPrice'] =  $item['unitPrice'];
                        $item['subtotal'] = $item['buyPrice'] * $item['buyCount'];
                    });
                    /* This is the same as the following foreach loop:
                    foreach ($cartitemList as &$item) {
                    $item['orderID'] = $orderID;
                    } */
                    DB::insert('orderitems', $cartitemList);
                }
                
                
                // 3. delete cartitems for this user's session (delete)
                DB::delete('cartitems', "sessionId=%s AND status=1", session_id());
                DB::commit();
                // TODO: send a confirmation email
                /*
                  $emailHtml = $app->view()->getEnvironment()->render('email_order.html.twig');
                  $headers = "MIME-Version: 1.0\r\n";
                  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                  mail($email, "Order " .$orderID . " placed ", $emailHtml, $headers);
                 */
                //
                return $view->render($response,'order_success.html.twig');
            } catch (MeekroDBException $e) {
                DB::rollback();
                db_error_handler(array(
                    'error' => $e->getMessage(),
                    'query' => $e->getQuery()
                ));
            }
        }
    }
});

$app->get('/orderlist', function(Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $buyerId = $_SESSION['user']['id'];
    $orderList = DB::query(
        "SELECT *"
        . " FROM orders "
        . " WHERE buyerId=%i", $buyerId);

    return $view->render($response, 'order_list.html.twig', ['orderList' => $orderList]);
    
});

$app->get('/orderdetail/{id:[0-9]+}', function(Request $request, Response $response, array $args ) {
    $view = Twig::fromRequest($request);
    $buyerId = $_SESSION['user']['id'];
    $id=$args['id'];
    $order = DB::queryFirstRow("SELECT * FROM orders WHERE buyerId=%i AND orders.id=%i", $buyerId, $id);
    $orderitemList = DB::query(
        "SELECT oi.*, p.name, p.unitPrice, p.picture"
        . " FROM orders, orderitems oi,products p"
        . " WHERE orders.id=oi.orderId AND oi.productId=p.id AND buyerId=%i AND orders.id=%i", $buyerId, $id);
    foreach($orderitemList as &$oi){
        $oi['picture'] = base64_encode($oi['picture']);
    }
    return $view->render($response, 'order_detail.html.twig', ['orderitemList' => $orderitemList, 'order' => $order]);
    
});

$app->get('/pay/{id:[0-9]+}', function(Request $request, Response $response, array $args ) {
    $view = Twig::fromRequest($request);
    $buyerId = $_SESSION['user']['id'];
    $id=$args['id'];
    $order = DB::queryFirstRow("SELECT * FROM orders WHERE buyerId=%i AND orders.id=%i", $buyerId, $id);
    
    return $view->render($response, 'payment.html.twig', ['order' => $order]);
    
});

$app->patch('/order/paid/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    
    $id = $args['id'];
    $method = $request->getMethod();
    // validate todo fields, 400 in case of error
    
    if (!DB::query("SELECT id FROM orders WHERE id=%s", $id)) {
        global $log;
        $result = "Record with id $id does not exist";
        $log->debug($method . " /auctions failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - " . $result));
        return $response;
    }
    $order['orderStatus']="paid";
    DB::update('orders', $order, "id=%s", $id);
    $buyer = DB::queryFirstRow("SELECT o.id as id, o.total as total, FirstName, LastName, u.email FROM orders o, users u WHERE o.buyerId=u.id AND o.id=%i",$id);
    //===========send email
    /*$cEmail = $_POST['payer_email'];
		$name = $_POST['first_name'] . " " . $_POST['last_name'];

		$price = $_POST['mc_gross'];
		$currency = $_POST['mc_currency'];
		$item = $_POST['item_number'];
		$paymentStatus = $_POST['payment_status'];*/
        /*Update credentials*/
	
			//$mail->send();
            require 'credential.php';
            require 'PHPMailerAutoload.php';
                //$cEmail = "33456229@qq.com";
                $cEmail = $buyer['email'];
                $name = $buyer['FirstName'] . $buyer['LastName'];
                $total = $buyer['total'];

                  
                    // $mail->SMTPDebug = SMTP::DEBUG_SERVER; // enable debug information out put
                    /*$mail->Host = 'localhost';
                    $mail->SMTPAuth = false;
                    $mail->SMTPAutoTLS = false; 
                    $mail->Port = 25; */
                    // Set mailer to use SMTP
                    //$mail->HOST = ' smtp.live.com'; // hotmail smtp server.
                    // $mail->SMTPDebug = SMTP::DEBUG_SERVER; // enable debug information out put
                    $mail = new PHPMailer;
                    $mail->isSMTP(); 
                    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;          // Enable SMTP authentication
                    $mail->Username = EMAIL;         // SMTP username
                    $mail->Password = PASS;          // SMTP password
                    $mail->SMTPSecure = 'tls';       // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = 587; 
                    //$mail->setFrom(EMAIL, "EMALL");
                    $mail->addAddress($cEmail, $name);
                    $mail->isHTML(true);
                    $mail->Subject = "Your Purchase Details of Order #".$id;
                    $mail->Body = "
                        Hi $name, <br><br>
                        Thank you for purchase.  <br><br>
                        Your order # $id amount $ $total has been received. <br><br>
                        Item(s) you ordered will be shipped in 1-3 days.<br><br>

                        Kind regards,<br><br>
                        E-Mall";
                    $mail->send();
                    /*if(!$mail->send()) {
                        echo 'Message could not be sent.';
                        echo 'Mailer Error: ' . $mail->ErrorInfo;
                    } else {
                        echo 'Message has been sent';
                    }	*/
    //===========end send email
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
    return $response;
});
// get product information for product review
$app->get('/api/product/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $id=$args['id'];
    $product = DB::queryFirstRow("SELECT products.id, name, picture, username AS sellerName FROM products, users WHERE products.sellerId=users.id AND products.id=%s", $id);
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $product['picture'] = base64_encode($product['picture']);
    
    $json = json_encode($product, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

// post review
$app->post('/reviews/{orderitemId:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    
    $json = $request->getBody();
    $review = json_decode($json, true); 
    // validate auction fields, 400 in case of error
    if ( ($result = validateReview($review)) !== TRUE) {
        global $log;
        $log->debug("POST /review failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    $orderitemId = $args['orderitemId'];
    $review['buyerId']=$_SESSION['user']['id'];
    try {
        DB::startTransaction();
        DB::insert('productreviews', $review);
        $id = DB::insertId();
        $ratingInfo = DB::queryFirstRow("SELECT rating, reviewCount FROM products WHERE id = %i", $review['productId']);
        $ratingInfo['rating'] = 
            ($ratingInfo['rating']*$ratingInfo['reviewCount']
            +$review['rating'])/($ratingInfo['reviewCount']+1);
        $ratingInfo['reviewCount'] = $ratingInfo['reviewCount'] + 1;

        DB::update('products', $ratingInfo, "id=%s", $review['productId']);
        DB::update('orderitems', ['reviewed' => '1'], "id=%s", $orderitemId);
        DB::commit();

        $response = $response->withStatus(201); // record created
        $response->getBody()->write(json_encode($id));
        return $response;
    } catch (MeekroDBException $e) {
        DB::rollback();
        db_error_handler(array(
            'error' => $e->getMessage(),
            'query' => $e->getQuery()
        ));
    }
});
function validateReview($review) {
    if ($review === NULL) { // if json_decode fails it returns null - handle it here
        return "Invalid JSON data provided";
    }
    $expectedFields = ['productId', 'review', 'rating', 'orderId'];
    $reviewFields = array_keys($review);
    // check for fields that should not be there
    if (($diff = array_diff($reviewFields, $expectedFields))) {
        return "Invalid fields in auction: [" . implode(',',$diff) . "]";
    }
    // check for fields that are missing
    if (($diff = array_diff($expectedFields, $reviewFields))) {
        return "Missing fields in auction: [" . implode(',',$diff) . "]";
    }
        
    if ($review['rating'] < 0 || $review['rating'] > 5) {
            return "Rating must be between 1-5";
    }
    // check if product exist in the order placed by the given buyer
    return TRUE;
}
$app->get('/product/{id:[0-9]+}', function(Request $request, Response $response, array $args ) {
    $view = Twig::fromRequest($request);
    $buyerId = isset($_SESSION['user']['id'])?$_SESSION['user']['id']:null;
    $id=$args['id'];
    $product = DB::queryFirstRow("SELECT p.*, u.username AS sellerName FROM products p, users u WHERE p.sellerId=u.id AND p.id=%i", $id);
    $similarList = DB::query("SELECT * FROM products WHERE categoryId=%s AND id!=%i LIMIT 7", $product['categoryId'], $id);
    foreach($similarList as &$similar){
        $similar['picture'] = base64_encode($similar['picture']);
    }
    $product['picture'] = base64_encode($product['picture']);
    $reviewList = DB::query("SELECT pr.*, u.username FROM productreviews pr, users u WHERE pr.buyerId=u.id AND productId=%i ORDER BY pr.id DESC", $id);
    foreach($reviewList as &$review){
        $starPercentage = ($review['rating'] / 5) * 100;
        // Round to nearest 10
        $starPercentageRounded = ($starPercentage / 10) * 10 . "%";
        $review['percentage']=$starPercentageRounded;
    }
    return $view->render($response, 'product_detail.html.twig', ['product' => $product, 'similarList' => $similarList, 'reviewList' => $reviewList]);
    
});
?>