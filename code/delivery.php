<?php
use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

require_once "setup.php";

$app->get('/delivery/', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    if( isset($_SESSION['deliveryManId']) ){
        $deliveryMan = DB::queryFirstRow("SELECT * FROM deliverymen 
        WHERE id= :s",$_SESSION['deliveryManId']);        
        return $view->render($response, 'delivery/index.html.twig' ,['deliveryMan' => $deliveryMan]);
    }else{
        return $response
        ->withHeader('Location', '/delivery/login');
    }
});

$app->get('/delivery/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['deliveryManId'])) {
        //TODO: Add loging message
        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'delivery/login.html.twig');
});

$app->post('/delivery/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    $loginInfo = $request->getParsedBody();

    if ($loginInfo != null) {
        if (isset($loginInfo['email']) && isset($loginInfo['password'])) {
            $deliveryMan = DB::queryFirstRow("SELECT * FROM deliverymen WHERE email= :s", $loginInfo['email']);
            if ($loginInfo['password'] === $deliveryMan['password']) {
                unset($deliveryMan['password']);
                $_SESSION['deliveryManId'] = $deliveryMan['id'];
                return $response
                    ->withHeader('Location', '/delivery/');
            }
        }
    }

    return $view->render($response, 'delivery/login.html.twig', [
        'error' => "Email doesn't match password."
    ]);
});

$app->get('/delivery/logout', function (Request $request, Response $response) {
    unset($_SESSION['deliveryManId']);
    return $response->withHeader('Location', '/delivery/login');
});

$app->get('/delivery/opentasks', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $orders = DB::query("SELECT O.*,                        
                            R.name AS restaurantName,R.address AS restaurantAddress,
                            R.city AS restaurantCity,R.province AS restaurantProvince,
                            R.country AS restaurantContry,R.phone AS restaurantPhone,R.shippingFee AS shippingFee,
                            U.address AS customerAddress,U.city AS customerCity,
                            U.province AS customerProvince,U.country AS customerContry,
                            U.phone AS customerPhone,U.name AS customerName,U.postcode AS customerPostcode
                        FROM orders AS O
                        INNER JOIN restaurants AS R
                            ON O.restaurantId = R.id
                        INNER JOIN users AS U
                            ON O.customerId = U.id
                        WHERE O.deliveryManId IS NULL AND status='pending'"
                        );
    return $view->render(
        $response,
        'delivery/opentasks.html.twig',
        ['orders' => $orders]
    );
});

$app->get('/delivery/mytasks', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $orders = DB::query("SELECT O.*,                        
                            R.name AS restaurantName,R.address AS restaurantAddress,
                            R.city AS restaurantCity,R.province AS restaurantProvince,
                            R.country AS restaurantContry,R.phone AS restaurantPhone,R.shippingFee AS shippingFee,
                            U.address AS customerAddress,U.city AS customerCity,
                            U.province AS customerProvince,U.country AS customerContry,
                            U.phone AS customerPhone,U.name AS customerName,U.postcode AS customerPostcode
                        FROM orders AS O
                        INNER JOIN restaurants AS R
                            ON O.restaurantId = R.id
                        INNER JOIN users AS U
                            ON O.customerId = U.id
                        WHERE O.deliveryManId=:i AND status='confirm'",$_SESSION['deliveryManId']
                        );
    return $view->render(
        $response,
        'delivery/mytasks.html.twig',
        ['orders' => $orders]
    );
});

$app->get('/delivery/deliveryhistory', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $orders = DB::query("SELECT O.*,R.name AS restaurantName,R.shippingFee AS shippingFee,U.name AS customerName
                        FROM orders AS O
                        INNER JOIN deliverymen AS D
                            ON O.deliveryManId = D.id
                        INNER JOIN restaurants AS R
                            ON O.restaurantId = R.id
                        INNER JOIN users AS U
                            ON O.customerId = U.id
                        WHERE O.deliveryManId=:i AND O.status='deliveried'", $_SESSION['deliveryManId']);
    return $view->render(
        $response,
        'delivery/deliveryhistory.html.twig',
        ['orders' => $orders]
    );
});

$app->PATCH('/api/delivery/phone', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
     global $log;
    $id = $_SESSION['deliveryManId'];
    $json = $request->getBody();
    $users = json_decode($json, true); // true makes it return an associative array instead of an object
    DB::query("update deliverymen set phone=:s where id=:s", $users['phone'], $id );
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses 
    return $response;
});

$app->PATCH('/api/delivery/address', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    //echo "test";
     global $log;
    $id = $_SESSION['deliveryManId'];
    $json = $request->getBody();
    $users = json_decode($json, true); // true makes it return an associative array instead of an object
    DB::query("update deliverymen set address=:s,city=:s,province=:s,country=:s,postcode=:s where id=:s", 
            $users['address'],$users['city'], $users['province'], $users['country'], $users['postcode'], $id );
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses 
    return $response;
});

$app->PATCH('/api/delivery/getTask', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
     global $log;
    $id = $_SESSION['deliveryManId'];
    $json = $request->getBody();
    $orders = json_decode($json, true); // true makes it return an associative array instead of an object
    DB::query("update orders set status='confirm',deliveryManId=':i' where id=:s",$id, $orders['id'] );
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses 
    return $response;        
});

$app->PATCH('/api/delivery/setDeliveried', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
     global $log;
    $id = $_SESSION['deliveryManId'];
    $json = $request->getBody();
    $orders = json_decode($json, true); // true makes it return an associative array instead of an object
    //var_dump($orders);
    //die();
    DB::query("update orders set status='deliveried',deliveryTime=NOW() where id=:s", $orders['id'] );
    DB::query("update deliverymen set balance= balance+:i where id=:s",$orders['shippingFee'], $id );
    DB::query("update restaurants set balance= balance+:i where id=:s",$orders['totalMoney'], $orders['restaurantId'] );
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses 
    return $response;        
});

$app->get('/delivery/changepassword', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'delivery/changepassword.html.twig');
});

$app->post('/delivery/changepassword', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $registerInfo = $request->getParsedBody();

    $password = $registerInfo['password'];

    $confirmPassword = $registerInfo['confirmPassword'];
    $passQuality = verifyPasswordQuality($password);
    if ($passQuality !== TRUE) {
        $errors['password'] = $passQuality;
    } elseif ($password !== $_POST['confirmPassword']) {
        $errors['password'] = "Passwords must be same.";
    }
    if (empty($errors)) {
        DB::query( "update deliverymen set password=:s where id=:s",$password, $_SESSION['deliveryManId'] );
        return $view->render($response, 'delivery/changepassword.html.twig', ['changeSuccess' => true] );
    }

    return $view->render($response, 'delivery/changepassword.html.twig', [
        'errors' => $errors,
    ]);
});