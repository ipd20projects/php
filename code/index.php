<?php
require_once "setup.php";

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Views\Twig;

// $app->get('/problem', function (Request $request, Response $response, array $args) {
//     DB::query("SELECT blah FROM blah");
// });

require_once "admin.php";

$app->get('/forbidden', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'website/index.html.twig');
});

require_once "account.php";
require_once "website.php";
require_once "restaurant.php";
require_once "delivery.php";
require_once "user.php";

$app->run();