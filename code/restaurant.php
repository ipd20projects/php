<?php

use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

use DI\Container;
use Psr\Http\Message\UploadedFileInterface;

require_once "setup.php";

$app->get('/restaurant/', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    if( isset($_SESSION['adminRestaurantId']) ){
        $restaurant = DB::queryFirstRow("SELECT * FROM restaurants 
        WHERE id= :s",$_SESSION['adminRestaurantId']);        
        return $view->render($response, 'restaurant/index.html.twig' ,['restaurant' => $restaurant]);
    }else{
        return $response
        ->withHeader('Location', '/restaurant/login');
    }
});

$app->get('/restaurant/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    if (isset($_SESSION['adminRestaurantId'])) {
        //TODO: Add loging message
        return $response->withHeader('Location', '/');
    }

    return $view->render($response, 'restaurant/login.html.twig');
});

$app->post('/restaurant/login', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);

    $loginInfo = $request->getParsedBody();

    if ($loginInfo != null) {
        if (isset($loginInfo['email']) && isset($loginInfo['password'])) {
            $restaurant = DB::queryFirstRow("SELECT * FROM restaurants WHERE email= :s", $loginInfo['email']);
            if ($loginInfo['password'] === $restaurant['password']) {
                unset($restaurant['password']);
                $_SESSION['adminRestaurantId'] = $restaurant['id'];
                return $response
                    ->withHeader('Location', '/restaurant/');
            }
        }
    }

    return $view->render($response, 'restaurant/login.html.twig', [
        'error' => "Email doesn't match password."
    ]);
});

$app->get('/restaurant/logout', function (Request $request, Response $response) {
    unset($_SESSION['adminRestaurantId']);
    return $response->withHeader('Location', '/restaurant/login');
});

$app->get('/restaurant/categories', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $categories = DB::query("SELECT * FROM categories WHERE restaurantId=:i", $_SESSION['adminRestaurantId']);
    return $view->render(
        $response,
        'restaurant/categories.html.twig',
        ['categories' => $categories]
    );
});

$app->get('/restaurant/menus', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $menus = DB::query("SELECT M.id AS id,M.name AS name,M.inventory AS inventory, 
                        M.price AS price,M.pictureFilePath AS picture,C.name AS categoryName 
                        FROM menus AS M
                        INNER JOIN categories AS C
                            ON M.categoryId = C.id
                        WHERE C.restaurantId=:i", $_SESSION['adminRestaurantId']);
    return $view->render(
        $response,
        'restaurant/menus.html.twig',
        ['menus' => $menus]
    );
});

$app->get('/restaurant/orders', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $orders = DB::query("SELECT O.*,U.name AS customerName,D.name AS deliveryMan
                        FROM orders AS O
                        LEFT JOIN deliverymen AS D
                            ON O.deliveryManId = D.id                         
                        INNER JOIN users AS U
                            ON O.customerId = U.id                           
                        WHERE O.restaurantId=:i ORDER BY O.orderDate DESC", $_SESSION['adminRestaurantId']);
    return $view->render(
        $response,
        'restaurant/orders.html.twig',
        ['orders' => $orders]
    );
});


$app->get('/restaurant/changepassword', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'restaurant/changepassword.html.twig');
});

$app->post('/restaurant/changepassword', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $registerInfo = $request->getParsedBody();

    $password = $registerInfo['password'];

    $confirmPassword = $registerInfo['confirmPassword'];
    $passQuality = verifyPasswordQuality($password);
    if ($passQuality !== TRUE) {
        $errors['password'] = $passQuality;
    } elseif ($password !== $_POST['confirmPassword']) {
        $errors['password'] = "Passwords must be same.";
    }
    if (empty($errors)) {
        DB::query( "update restaurants set password=:s where id=:s",$password, $_SESSION['adminRestaurantId'] );
        return $view->render($response, 'restaurant/changepassword.html.twig', ['changeSuccess' => true] );
    }

    return $view->render($response, 'restaurant/changepassword.html.twig', [
        'errors' => $errors,
    ]);
});

$app->get('/restaurant/categories/add', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render(
        $response,'restaurant/addcategory.html.twig'
    );
});

$app->post('/restaurant/categories/add', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $registerInfo = $request->getParsedBody();
    $name = $registerInfo['name'];
    $description = $registerInfo['description'];    
    if (strlen($name) < 5 || strlen($name) > 30) {
        $errors['name'] = "Name must be 5~30 chars";
        $registerInfo['name'] = '';
    }
    if (strlen($description) < 5 || strlen($description) > 200) {
        $errors['description'] = "description must be 5~200 chars";
        $registerInfo['description'] = '';
    }    

    if (empty($errors)) {
        DB::insert('categories', [
            'name' => $name,
            'description' => $description,
            'restaurantId' => $_SESSION['adminRestaurantId'],
        ]);
        return $response
        ->withHeader('Location', '/restaurant/categories');
    }

    return $view->render($response, 'restaurant/addcategory.html.twig', [
        'errors' => $errors,
        'prevInput' => [
            'name' => $name,
            'description' => $description,
        ]
    ]);    

});

$app->delete('/api/categories/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $id = $args['id'];
    DB::delete('categories', 'id=:s AND restaurantId=:s', $id, $_SESSION['adminRestaurantId']);
    $count = DB::affectedRows();
    $json = json_encode($count != 0, JSON_PRETTY_PRINT); // returns true/false
    $response->getBody()->write($json);
    return $response;
});

$app->get('/restaurant/categories/edit', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $paramInfo = $request->getQueryParams();
    $id = $paramInfo['id'];
    $category = DB::queryFirstRow("SELECT * FROM categories WHERE id=:i",$id );
    return $view->render(
        $response,
        'restaurant/addcategory.html.twig',
        ['category' => $category]
    );
});

$app->post('/restaurant/categories/edit', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $paramInfo = $request->getParsedBody();
    $id = $paramInfo['id'];
    $name = $paramInfo['name'];
    $description = $paramInfo['description'];    
    if (strlen($name) < 5 || strlen($name) > 30) {
        $errors['name'] = "Name must be 5~30 chars";
        $registerInfo['name'] = '';
    }
    if (strlen($description) < 1 || strlen($description) > 200) {
        $errors['description'] = "description must be 5~200 chars";
        $registerInfo['description'] = '';
    }    

    if (empty($errors)) {
        // DB::insert('categories', [
        //     'name' => $name,
        //     'description' => $description,
        //     'restaurantId' => $_SESSION['adminRestaurantId'],
        // ]);
        DB::query("update categories set name=:s,description=:s where id=:s", $name, $description, $id );        
        return $response
        ->withHeader('Location', '/restaurant/categories');
    }

    return $view->render($response, 'restaurant/addcategory.html.twig', [
        'errors' => $errors,
        'prevInput' => [
            'name' => $name,
            'description' => $description,
        ]
    ]);    
});

$app->get('/restaurant/menus/add', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $categories = DB::query("SELECT * FROM categories WHERE restaurantId=:i", $_SESSION['adminRestaurantId']);    
    return $view->render(
        $response,'restaurant/addmenu.html.twig', [
            'categories' => $categories,
        ]
    );
});

$app->post('/restaurant/menus/add', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $paramInfo = $request->getParsedBody();
    $name = $paramInfo['name'];
    $price = $paramInfo['price'];
    $categoryId = $paramInfo['categoryId'];
    $errors = [];
    if (strlen($name) < 5 || strlen($name) > 30) {
        $errors['name'] = "Name must be 5~30 chars";
        $paramInfo['name'] = '';
    }
    if ($price < 0) {
        $errors['price'] = "price must be greater than 0";
        $paramInfo['price'] = '';
    }    

    $categories = DB::query("SELECT * FROM categories WHERE restaurantId=:i", $_SESSION['adminRestaurantId']);        

	//for upload picture
    $directory = $this->get('upload_directory');
    $directory = $directory."/menus";
    $uploadedFiles = $request->getUploadedFiles();

    // handle single input with single file upload
    $uploadedFile = $uploadedFiles['photo'];    

    $fileExtension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $fileExtension = strtoupper($fileExtension);
    if($fileExtension!="JPG" && $fileExtension!="GIF" && $fileExtension!="PNG")
    {
        $errors['photo'] = "Uploaded file only can be jpg, png, gif";
    }  

    if(!empty($errors)){
        return $view->render($response, 'restaurant/addmenu.html.twig', [
            'prevInput' => [
                'name' => $name,
                'price' => $price,
            ],'errors' => $errors,'categories' => $categories
        ]);
    }else{    
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile($directory, $uploadedFile);
            $response->getBody()->write('uploaded ' . $filename . '<br/>');
        } else {
            $response->getBody()->write('upload failed');
        }
        $filenamePath = $filename;

        DB::insert('menus',['name'=>$name, 'price'=>$price,'categoryId'=>$categoryId,'pictureFilePath'=>$filenamePath]);
        return $response
         ->withHeader('Location', '/restaurant/menus');
    } 
});

$app->delete('/api/menus/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $id = $args['id'];
    DB::delete('menus', 'id=:s', $id);
    $count = DB::affectedRows();
    $json = json_encode($count != 0, JSON_PRETTY_PRINT); // returns true/false
    $response->getBody()->write($json);
    return $response;
});

$app->get('/restaurant/menus/edit', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $paramInfo = $request->getQueryParams();
    $id = $paramInfo['id'];
    $menu = DB::queryFirstRow("SELECT * FROM menus WHERE id=:i",$id );
    $categories = DB::query("SELECT * FROM categories WHERE restaurantId=:i", $_SESSION['adminRestaurantId']);    
    
    return $view->render(
        $response,
        'restaurant/addmenu.html.twig',
        [
            'menu' => $menu,'categories'=>$categories
        ]
    );
});

$app->post('/restaurant/menus/edit', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $paramInfo = $request->getParsedBody();
    $id = $paramInfo['id'];
    $name = $paramInfo['name'];
    $price = $paramInfo['price'];
    $categoryId = $paramInfo['categoryId'];
    $errors = [];
    if (strlen($name) < 1 || strlen($name) > 30) {
        $errors['name'] = "Name must be 5~30 chars";
        $paramInfo['name'] = '';
    }
    if ($price < 0) {
        $errors['price'] = "price must be greater than 0";
        $paramInfo['price'] = '';
    }    

    $categories = DB::query("SELECT * FROM categories WHERE restaurantId=:i", $_SESSION['adminRestaurantId']);        

	//for upload picture
    $directory = $this->get('upload_directory');
    $directory = $directory."/menus";
    $uploadedFiles = $request->getUploadedFiles();

    // handle single input with single file upload
    $uploadedFile = $uploadedFiles['photo'];    

    $fileExtension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $fileExtension = strtoupper($fileExtension);
    if($fileExtension!="JPG" && $fileExtension!="GIF" && $fileExtension!="PNG")
    {
        $errors['photo'] = "Uploaded file only can be jpg, png, gif";
    }  

    if(!empty($errors)){
        return $view->render($response, 'restaurant/addmenu.html.twig', [
            'prevInput' => [
                'name' => $name,
                'price' => $price,
            ],'errors' => $errors,'categories' => $categories
        ]);
    }else{    
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile($directory, $uploadedFile);
            $response->getBody()->write('uploaded ' . $filename . '<br/>');
        } else {
            $response->getBody()->write('upload failed');
        }
        $filenamePath = $filename;

       // DB::insert('menus',['name'=>$name, 'price'=>$price,'categoryId'=>$categoryId,'pictureFilePath'=>$filenamePath]);
        DB::query("update menus set name=:s,price=:s,categoryId=:s,pictureFilePath=:s where id=:s", $name,$price,$categoryId, $filenamePath, $id );        
        return $response
        ->withHeader('Location', '/restaurant/menus');        
    } 
});

$app->get('/api/orderdetail', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $paramArray = $request->getQueryParams();
    $orderNumber = $paramArray['orderNumber'];
    $itemList = DB::query("SELECT OD.*,M.name AS menuName 
                    FROM orderitems AS OD
                        INNER JOIN menus AS M
                            ON OD.menuId = M.id
                    WHERE orderNumber=:s ", $orderNumber);
    if (!$itemList) { // not found
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    }
    $json = json_encode($itemList, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

function moveUploadedFile($directory, UploadedFileInterface $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}