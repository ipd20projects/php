<?php
use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

require_once "setup.php";

$app->get('/user/', function (Request $request, Response $response) {

    $view = Twig::fromRequest($request);
    if( isset($_SESSION['user']) ){
        $user = DB::queryFirstRow("SELECT * FROM users 
        WHERE id= :s",$_SESSION['user']['id']);        
        return $view->render($response, 'user/index.html.twig',['user' => $user]);
    }else{
        return $response
        ->withHeader('Location', '/login');
    }    

/*
    $view = Twig::fromRequest($request);
    $email = "shuixiutan@gmail.com";
    $user = DB::queryFirstRow("SELECT * FROM users 
                                   WHERE email= :s",$email);
    $_SESSION['email'] = $email;                             
    $_SESSION['userId'] = $user['id']; 
    return $view->render($response, 'user/index.html.twig',['user' => $user]);

*/
});


$app->get('/user/orderhistory', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $orders = DB::query("SELECT O.*,R.name AS restaurantName,R.shippingFee AS shippingFee, U.name AS customerName
                        FROM orders AS O
                        LEFT JOIN deliverymen AS D
                            ON O.deliveryManId = D.id
                        INNER JOIN restaurants AS R
                            ON O.restaurantId = R.id
                        INNER JOIN users AS U
                            ON O.customerId = U.id
                        WHERE O.customerId=:i ", $_SESSION['user']['id']);
    //var_dump($orders);
    return $view->render(
        $response,
        'user/orderhistory.html.twig',
        ['orders' => $orders]
    );
});

$app->PATCH('/api/users/phone', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
     global $log;
    $id = $_SESSION['userId'];
    $json = $request->getBody();
    $users = json_decode($json, true); // true makes it return an associative array instead of an object
    DB::query("update users set phone=:s where id=:s", $users['phone'], $id );
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses 
    return $response;
});

$app->PATCH('/api/users/address', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    //echo "test";
     global $log;
    $id = $_SESSION['userId'];
    $json = $request->getBody();
    $users = json_decode($json, true); // true makes it return an associative array instead of an object
    DB::query("update users set address=:s,city=:s,province=:s,country=:s,postcode=:s where id=:s", 
            $users['address'],$users['city'], $users['province'], $users['country'], $users['postcode'], $id );
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses 
    return $response;
});

$app->get('/user/changepassword', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'user/changepassword.html.twig');
});

$app->post('/user/changepassword', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    $registerInfo = $request->getParsedBody();

    $password = $registerInfo['password'];

    $confirmPassword = $registerInfo['confirmPassword'];
    $passQuality = verifyPasswordQuality($password);
    if ($passQuality !== TRUE) {
        $errors['password'] = $passQuality;
    } elseif ($password !== $_POST['confirmPassword']) {
        $errors['password'] = "Passwords must be same.";
    }
    if (empty($errors)) {
        DB::query( "update users set password=:s where id=:s",$password, $_SESSION['userId'] );
        return $view->render($response, 'user/changepassword.html.twig', ['changeSuccess' => true] );
    }

    return $view->render($response, 'user/changepassword.html.twig', [
        'errors' => $errors,
    ]);
});

