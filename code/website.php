<?php

use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

require_once "setup.php";

$app->get('/chooseRestaurant', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    global $log;
    $paramArray = $request->getQueryParams();
    $postcode = isset($paramArray['postcode']) ? $paramArray['postcode'] : "H9R 4Y5";
    $arrCenterPostcode = getLatLong($postcode);
    $arrCenterPostcode['postcode'] = $postcode;
    $_SESSION['arrCenterPostcode'] = $arrCenterPostcode;    
    if ($postcode || $_SESSION['arrCenterPostcode']) {
        $restaurantsList = DB::query("SELECT * FROM restaurants WHERE pictureFilePath IS NOT NULL");
        $index = 1;
        foreach ($restaurantsList as &$restaruant) {
            $arrLatLong = getLatLong($restaruant['postcode']);
            $restaruant['index'] = $index++;
            $restaruant['Latitude'] = $arrLatLong['Latitude'];
            $restaruant['Longitude'] = $arrLatLong['Longitude'];
        }
        return $view->render(
            $response,
            'website/chooseRestaurant.html.twig',
            [
                'restaurantsList' => $restaurantsList,
                'arrCenterPostcode' => $arrCenterPostcode
            ]
        );
    } else {
        return $view->render($response, 'website/chooseRestaurant.html.twig');
    }
});

$app->get('/chooseMenu/{id}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $restaurantId = isset($args['id']) ? $args['id'] : "";
    $_SESSION['restaurantId'] = $restaurantId;
    $restaurant = DB::queryFirstRow("SELECT * FROM restaurants WHERE id=:s", $restaurantId);
    $categories = DB::query("SELECT * FROM categories WHERE restaurantId = :i", $restaurantId);
    $cartItems = DB::query("SELECT C.id AS id, M.name AS menuName,M.price AS price,
                            C.quantity AS quantity,C.restaurantId AS restaurantId  
                            FROM cartitems AS C
                            INNER JOIN menus AS M
                                ON C.menuId = M.id
                            WHERE restaurantId = :i AND sessionId = :s", $restaurantId, session_id());
    

    $totalMoney =  DB::queryFirstField("SELECT SUM(M.price*C.quantity) AS totalMoney
                            FROM cartitems AS C
                            INNER JOIN menus AS M
                                ON C.menuId = M.id
                            WHERE restaurantId=:s AND sessionId=:s", $restaurantId,session_id());
    
    $menus = DB::query("SELECT M.id AS id,M.categoryId AS categoryId,M.name AS name,
                            M.pictureFilePath AS pictureFilePath,C.id AS categoryId, 
                            M.price AS price, C.name AS categoryName,C.restaurantId AS restaurantId 
                            FROM menus AS M 
                            INNER JOIN categories AS C
                                ON C.id = M.categoryId
                            WHERE C.restaurantId = :i
                            ", $restaurantId);
    $index = 1;
    foreach ($menus as &$menu) {
        $menu['index'] = $index++;
    }
    return $view->render(
        $response,
        'website/chooseMenu.html.twig',
        [
            'categories' => $categories,
            'menus' => $menus,
            'restaurantPicPath' =>$restaurant['pictureFilePath'],
            'restaurant' =>$restaurant,
            'cartItems' => $cartItems,
            'totalMoney'=> $totalMoney
        ]
    );
});

$app->get('/api/cartitems', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $paramArray = $request->getQueryParams();
    $cartItems = DB::query("SELECT C.id AS id, M.name AS menuName,M.price AS price,
                            C.quantity AS quantity,C.restaurantId AS restaurantId  
                            FROM cartitems AS C
                            INNER JOIN menus AS M
                                ON C.menuId = M.id
                            WHERE restaurantId = :i AND sessionId = :s", $_SESSION['restaurantId'], session_id());
    $json = json_encode($cartItems, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

$app->get('/cart/add', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $sessionId = session_id();
    $get = $request->getQueryParams();
    $message = 'failed';
    if (isset($get['menuId'])) {
        $menuId = $get['menuId'];
        $quantity = $get['quantity'];
        $cartItem = DB::queryFirstRow(
            "SELECT quantity 
                    FROM cartitems 
                    WHERE sessionId = :s
                    AND menuId = :i
                    AND restaurantId = :i",
            $sessionId,
            $menuId,
            $_SESSION['restaurantId']
        );
        if ($cartItem != null) {
            DB::update('cartitems', array(
                'sessionID' => $sessionId,
                'menuId' => $menuId,
                'quantity' => $cartItem['quantity'] + $quantity
            ), "menuId=:d AND sessionID=:s", $menuId, $sessionId);

            $message = "succeed";
        } else {
            DB::insert("cartitems", [
                'sessionId' => $sessionId,
                'menuId' => $menuId,
                'quantity' => $quantity
            ]);
            $message = "succeed";
        }
        $restaurantPicPath = DB::queryFirstField("SELECT pictureFilePath FROM restaurants WHERE id=:s", $_SESSION['restaurantId']);
        $categories = DB::query("SELECT * FROM categories WHERE restaurantId = :i", $_SESSION['restaurantId']);
        $cartItems = DB::query("SELECT M.name AS menuName,M.price AS menuPrice,C.quantity AS quantity  
                                FROM cartitems AS C
                                INNER JOIN menus AS M
                                    ON C.menuId = M.id
                                WHERE restaurantId = :i AND sessionId = :s", $_SESSION['restaurantId'], $sessionId);
        $menus = DB::query("SELECT M.id AS id,M.categoryId AS categoryId,M.name AS name,
                                M.pictureFilePath AS pictureFilePath,C.id AS categoryId, 
                                M.price AS price, C.name AS categoryName,C.restaurantId AS restaurantId 
                                FROM menus AS M 
                                INNER JOIN categories AS C
                                    ON C.id = M.categoryId
                                WHERE C.restaurantId = :i
                                ", $_SESSION['restaurantId']);
        // var_dump($cartItems);    
        // die;   
        $index = 1;
        foreach ($menus as &$menu) {
            $menu['index'] = $index++;
        }
        return $view->render(
            $response,
            'website/chooseMenu.html.twig',
            ['categories' => $categories, 'cartItems' => $cartItems, 'menus' => $menus, 'restaurantPicPath' => $restaurantPicPath]
        );
    } else {
        $response->getBody()->write($message);
        return $response;
    }
});

$app->get('/cart/delete', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $sessionId = session_id();
    $get = $request->getQueryParams();
    $message = 'failed';
    if (isset($get['menuId'])) {
        $menuId = $get['menuId'];
        //$restaurantId = $get['restaurantId'];
        DB::delete('cartitems', 'menuId=:i AND sessionId=:s AND restaurantId=:i', $menuId, $sessionId, $_SESSION['restaurantId']);
        $message = "succeed";
        $restaurantPicPath = DB::queryFirstField("SELECT pictureFilePath FROM restaurants WHERE id=:s", $_SESSION['restaurantId']);
        $categories = DB::query("SELECT * FROM categories WHERE restaurantId = :i", $_SESSION['restaurantId']);
        $cartItems = DB::query("SELECT M.name AS menuName,M.price AS menuPrice,C.quantity AS quantity  
                                FROM cartitems AS C
                                INNER JOIN menus AS M
                                    ON C.menuId = M.id
                                WHERE restaurantId = :i AND sessionId = :s", $_SESSION['restaurantId'], $sessionId);
        $menus = DB::query("SELECT M.id AS id,M.categoryId AS categoryId,M.name AS name,
                                M.pictureFilePath AS pictureFilePath,C.id AS categoryId, 
                                M.price AS price, C.name AS categoryName,C.restaurantId AS restaurantId 
                                FROM menus AS M 
                                INNER JOIN categories AS C
                                    ON C.id = M.categoryId
                                WHERE C.restaurantId = :i
                                ", $_SESSION['restaurantId']);
        // var_dump($cartItems);    
        // die;   
        $index = 1;
        foreach ($menus as &$menu) {
            $menu['index'] = $index++;
        }
        return $view->render(
            $response,
            'website/chooseMenu.html.twig',
            ['categories' => $categories, 'cartItems' => $cartItems, 'menus' => $menus, 'restaurantPicPath' => $restaurantPicPath]
        );
    } else {
        $response->getBody()->write($message);
        return $response;
    }
});

$app->get('/placeorder/{totalMoney}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $totalMoney = isset($args['totalMoney']) ? $args['totalMoney'] : "";
    $cartitems = DB::query("SELECT * FROM cartitems 
                            WHERE  restaurantId = :i
                            AND  sessionId = :s", $_SESSION['restaurantId'], session_id());
    $maxId = DB::queryFirstField("SELECT max(id) as maxId FROM orders");
    if ($maxId == NULL) {
        $maxId = 0;
    }
    $date = date('YmdHi');
    //create ordernumbe by today + max id +1

    $orderNumber = (string) ($date) . (string) ($maxId + 1);
    //var_dump($orderNumber);  
    //$totalMeney = $requestInfo['totalMeney'];
    DB::insert('orders', [
        'customerId' => $_SESSION['user']['id'],
        'orderNumber' => $orderNumber,
        'restaurantId' => $_SESSION['restaurantId'],
        'totalMoney' => $totalMoney
    ]);

    foreach ($cartitems as &$cartitem) {
        DB::insert('orderitems', [
            'menuId' => $cartitem['menuId'],
            'orderNumber' => $orderNumber,
            'quantity' => $cartitem['quantity'],
        ]);
        DB::delete(
            'cartitems',
            'menuId=:i AND sessionId=:s AND restaurantId=:i',
            $cartitem['menuId'],
            session_id(),
            $_SESSION['restaurantId']
        );
    }
    return $view->render($response, 'website/placeorder.html.twig', ['orderNumber' => $orderNumber]);
});

$app->post('/orderview', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $checkoutInfo = $request->getParsedBody();
    $lastFourCardNum = substr($checkoutInfo['cardNumber'], -4);
    $checkoutInfo['lastFourCardNum'] = $lastFourCardNum;
    $cartItems = DB::query("SELECT M.name AS menuName,M.price AS price,M.pictureFilePath AS pictureFilePath,
                            C.quantity AS quantity  FROM cartitems AS C
                            INNER JOIN menus AS M
                            ON C.menuId = M.id
                            WHERE restaurantId = :i 
                            AND sessionId = :s", $_SESSION['restaurantId'], session_id());
    $subTotal = DB::queryFirstField("SELECT sum(M.price) AS subTotal FROM cartitems AS C
                            INNER JOIN menus AS M
                            ON C.menuId = M.id
                            WHERE restaurantId = :i 
                            AND sessionId = :s", $_SESSION['restaurantId'], session_id());
    $shippingFee = DB::queryFirstField("SELECT shippingFee FROM restaurants
                            WHERE id = :i", $_SESSION['restaurantId']);

    if (isset($_SESSION['user'])) {
        $users = DB::query("SELECT * FROM users WHERE id = :i",  $_SESSION['user']['id']);
    }
    return $view->render(
        $response,
        'website/orderview.html.twig',
        [
            'cartItems' => $cartItems,
            'subTotal' => $subTotal,
            'checkoutInfo' => $checkoutInfo,
            'shippingFee' => $shippingFee,
            'users' => $users['0']
        ]
    );
});

$app->get('/checkout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $cartItems = DB::query("SELECT M.name AS menuName,M.price AS price,M.pictureFilePath AS pictureFilePath,
                            C.quantity AS quantity,C.restaurantId AS restaurantId FROM cartitems AS C
                            INNER JOIN menus AS M
                                ON C.menuId = M.id
                            WHERE C.restaurantId = :i 
                            AND sessionId = :s", $_SESSION['restaurantId'], session_id());
    $subTotal = DB::queryFirstField("SELECT sum(M.price) AS subTotal FROM cartitems AS C
                            INNER JOIN menus AS M
                            ON C.menuId = M.id
                            WHERE restaurantId = :i 
                            AND sessionId = :s", $_SESSION['restaurantId'], session_id());
    $shippingFee = DB::queryFirstField("SELECT shippingFee FROM restaurants
                            WHERE id = :i", $_SESSION['restaurantId']);

    //var_dump($cartItems);
    //die;
    if (isset($_SESSION['user'])) {
        $users = DB::query("SELECT * FROM users WHERE id = :i",  $_SESSION['user']['id']);
    }
    return $view->render(
        $response,
        'website/checkout.html.twig',
        [
            'cartItems' => $cartItems,
            'subTotal' => $subTotal,
            'shippingFee' => $shippingFee,
            'users' => $users['0']
        ]
    );
});

function getLatLong($code)
{
    //https://maps.googleapis.com/maps/api/geocode/json?&address=h9r4y5&key=AIzaSyBON6db-E8cIzVii1TuqKult0KLq9zvrDE
    $mapsApiKey = 'AIzaSyBON6db-E8cIzVii1TuqKult0KLq9zvrDE';
    $query = "https://maps.googleapis.com/maps/api/geocode/json?&address=" . urlencode($code) . "&key=" . $mapsApiKey;
    $data = file_get_contents($query);
    //var_dump($data);
    //die();
    // if data returned
    if ($data) {
        // convert into readable format
        $data = json_decode($data);

        $lat = $data->results[0]->geometry->location->lat;
        $long = $data->results[0]->geometry->location->lng;
        return array('Latitude' => $lat, 'Longitude' => $long);
    } else {
        return false;
    }
}
