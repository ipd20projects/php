-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: May 06, 2020 at 02:49 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `letseat`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartitems`
--

CREATE TABLE `cartitems` (
  `id` int(11) NOT NULL,
  `menuId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `addedTS` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cartitems`
--

INSERT INTO `cartitems` (`id`, `menuId`, `quantity`, `sessionId`, `addedTS`) VALUES
(1, 1, 11, 'v7hup47h2c5dnjd94g2tq2tcao', '2020-05-06 05:10:31'),
(2, 4, 1, 'v7hup47h2c5dnjd94g2tq2tcao', '2020-05-06 05:20:07'),
(3, 5, 1, 'v7hup47h2c5dnjd94g2tq2tcao', '2020-05-06 05:21:29');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `restaurantId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `restaurantId`) VALUES
(1, 'Salade', 2),
(2, 'Chicken', 2);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `addedTS` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `content` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(60) NOT NULL,
  `city` varchar(15) NOT NULL,
  `province` varchar(10) NOT NULL,
  `country` varchar(15) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `email` varchar(255) NOT NULL,
  `registerDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `username` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deliveryemails`
--

CREATE TABLE `deliveryemails` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `sendedTS` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `confirmStatus` enum('pending','confirm','cancel') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deliverymen`
--

CREATE TABLE `deliverymen` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `balance` decimal(10,0) NOT NULL,
  `address` varchar(60) NOT NULL,
  `city` varchar(15) NOT NULL,
  `province` varchar(10) NOT NULL,
  `country` varchar(15) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `email` varchar(255) NOT NULL,
  `registerDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `username` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL,
  `workType` enum('partTime','fullTime') NOT NULL,
  `vehicleRegistration` varchar(20) NOT NULL,
  `vehicleYear` year(4) NOT NULL,
  `driverLisence` varchar(20) NOT NULL,
  `driveYears` int(11) NOT NULL,
  `lisencePlate` varchar(20) NOT NULL,
  `driverLisPicPath` varchar(100) NOT NULL,
  `SIN` int(11) NOT NULL,
  `status` enum('on','off') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `menuitems`
--

CREATE TABLE `menuitems` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `pictureFilePath` varchar(100) NOT NULL,
  `inventory` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menuitems`
--

INSERT INTO `menuitems` (`id`, `name`, `price`, `pictureFilePath`, `inventory`, `categoryId`) VALUES
(1, 'Kafta Arayes', '10.00', 'food1.JPG', 100, 2),
(4, 'Double Chicken Leg\r\n', '16.50', 'food2.JPG', 100, 2),
(5, 'Chicken Wings', '15.00', 'food3.JPG', 100, 2),
(6, 'Cream of chicken', '4.50', 'food4.JPG', 100, 2),
(7, 'Half Chicken', '18.50', 'food5.JPG', 100, 2),
(8, 'Rotisserie Chicken Breast', '15.00', 'food6.JPG', 100, 2);

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `foodItemId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `menuItemId` int(11) NOT NULL,
  `deliverMenId` int(11) NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `totalMoney` decimal(10,0) NOT NULL,
  `deliverType` enum('deliver','pickup') NOT NULL,
  `status` enum('1','2','3','4') NOT NULL,
  `expectDeliverTime` datetime NOT NULL,
  `deliveryTime` timestamp NULL DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `deliveryFee` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `balance` decimal(10,0) NOT NULL,
  `address` varchar(60) NOT NULL,
  `city` varchar(15) NOT NULL,
  `province` varchar(10) NOT NULL,
  `country` varchar(15) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `email` varchar(255) NOT NULL,
  `registerDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `username` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL,
  `pictureFilePath` varchar(100) NOT NULL,
  `rate` float NOT NULL,
  `openTime` time NOT NULL,
  `closedTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `name`, `owner`, `balance`, `address`, `city`, `province`, `country`, `postcode`, `phone`, `email`, `registerDate`, `username`, `password`, `pictureFilePath`, `rate`, `openTime`, `closedTime`) VALUES
(1, 'Moushi', 'Moushi\r\n', '0', '3593 Boul Saint-Jean', 'Pointe Claire', 'QC', 'Canada', 'H9G 1W9', '514-624-3663', 'Moushi@gmail.com', '2020-05-03 19:07:59', 'shuixiutan', '123456', 'restaruant1.JPG', 9.5, '00:00:00', '00:00:00'),
(2, 'Quesada Burritos & Tacos', 'shuixiutan', '0', '3343 Boulevard des Sources', 'Pointe Claire', 'QC', 'Canada', 'H9B 1Z8', '514-624-3663', 'Quesada@gmail.com', '2020-05-03 19:13:51', 'shuixiutan', '123456', 'restaruant2.JPG', 8.5, '00:00:00', '00:00:00'),
(3, 'Toasties', 'Toasties', '0', '4712 Boul Saint-Jean', 'Pointe Claire', 'QC', 'Canada', 'H9H 4B2', '514-624-3663', 'Toasties@gmail.com', '2020-05-03 19:18:05', 'shuixiutan', '123456', 'restaruant3.JPG', 8.3, '00:00:00', '00:00:00'),
(4, 'Dairy Queen', 'shuixiutan', '0', '4501 Boulevard Saint-Charles', 'Pointe Claire', 'QC', 'Canada', 'H9H 3C7', '514-624-3663', 'shuixiutan@gmail.com', '2020-05-03 19:20:29', 'shuixiutan', '123456', 'restaruant4.JPG', 7.9, '00:00:00', '00:00:00'),
(5, 'Kinton Ramen', 'shuixiutan', '0', '303a Boulevard Brunswick', 'Pointe Claire', 'QC', 'Canada', 'H9R 4Y2', '514-624-3663', 'shuixiutan@gmail.com', '2020-05-03 19:22:16', 'shuixiutan', '123456', 'restaruant5.JPG', 8.8, '00:00:00', '00:00:00'),
(6, 'Lahore Karahi Restaurant', 'shuixiutan', '0', '14329 Boul de Pierrefonds', 'Pointe Claire', 'QC', 'Canada', 'H9H 1Z2', '514-624-3663', 'shuixiutan@gmail.com', '2020-05-03 19:24:07', 'shuixiutan', '123456', 'restaruant6.JPG', 9.2, '00:00:00', '00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foodItemId` (`menuId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restaurantId` (`restaurantId`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderId` (`orderId`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliveryemails`
--
ALTER TABLE `deliveryemails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderId` (`orderId`);

--
-- Indexes for table `deliverymen`
--
ALTER TABLE `deliverymen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menuitems`
--
ALTER TABLE `menuitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryId` (`categoryId`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foodItemId` (`foodItemId`),
  ADD KEY `orderId` (`orderId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customerId` (`customerId`),
  ADD KEY `deliverMenId` (`deliverMenId`),
  ADD KEY `foodItemId` (`menuItemId`);

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cartitems`
--
ALTER TABLE `cartitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliveryemails`
--
ALTER TABLE `deliveryemails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliverymen`
--
ALTER TABLE `deliverymen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menuitems`
--
ALTER TABLE `menuitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD CONSTRAINT `cartitems_ibfk_1` FOREIGN KEY (`menuId`) REFERENCES `menuitems` (`id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Constraints for table `deliveryemails`
--
ALTER TABLE `deliveryemails`
  ADD CONSTRAINT `deliveryemails_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Constraints for table `menuitems`
--
ALTER TABLE `menuitems`
  ADD CONSTRAINT `menuitems_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`);

--
-- Constraints for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD CONSTRAINT `orderitems_ibfk_1` FOREIGN KEY (`foodItemId`) REFERENCES `menuitems` (`id`),
  ADD CONSTRAINT `orderitems_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`deliverMenId`) REFERENCES `deliverymen` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`menuItemId`) REFERENCES `menuitems` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
