-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: May 12, 2020 at 06:53 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `letseat`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartitems`
--

CREATE TABLE `cartitems` (
  `id` int(11) NOT NULL,
  `menuId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `addedTS` timestamp NOT NULL DEFAULT current_timestamp(),
  `restaurantId` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cartitems`
--

INSERT INTO `cartitems` (`id`, `menuId`, `quantity`, `sessionId`, `addedTS`, `restaurantId`) VALUES
(57, 1, 1, 'sqm5i17asks8dru6vl8jpl830q', '2020-05-11 04:52:09', 1),
(58, 4, 1, 'sqm5i17asks8dru6vl8jpl830q', '2020-05-11 04:52:10', 1),
(59, 5, 1, 'sqm5i17asks8dru6vl8jpl830q', '2020-05-11 04:52:12', 1),
(65, 1, 1, 'h7oql365mk2nce1c8e7jq2h6em', '2020-05-12 04:33:20', 1),
(66, 4, 1, 'h7oql365mk2nce1c8e7jq2h6em', '2020-05-12 04:33:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `restaurantId`, `description`) VALUES
(1, 'Salade', 2, ''),
(2, 'Chicken', 1, 'Very delicious'),
(3, 'Drink', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `addedTS` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `content` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deliveryemails`
--

CREATE TABLE `deliveryemails` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `sendedTS` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `confirmStatus` enum('pending','confirm','cancel') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deliverymen`
--

CREATE TABLE `deliverymen` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `balance` decimal(10,0) NOT NULL,
  `address` varchar(60) NOT NULL,
  `city` varchar(15) NOT NULL,
  `province` varchar(10) NOT NULL,
  `country` varchar(15) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `email` varchar(255) NOT NULL,
  `registerDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `password` varchar(64) NOT NULL,
  `workType` enum('partTime','fullTime') NOT NULL,
  `vehicleRegistration` varchar(20) NOT NULL,
  `vehicleYear` year(4) NOT NULL,
  `driverLisence` varchar(20) NOT NULL,
  `driveYears` int(11) NOT NULL,
  `lisencePlate` varchar(20) NOT NULL,
  `driverLisPicPath` varchar(100) NOT NULL,
  `SIN` int(11) NOT NULL,
  `status` enum('on','off') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `deliverymen`
--

INSERT INTO `deliverymen` (`id`, `name`, `age`, `balance`, `address`, `city`, `province`, `country`, `postcode`, `phone`, `email`, `registerDate`, `password`, `workType`, `vehicleRegistration`, `vehicleYear`, `driverLisence`, `driveYears`, `lisencePlate`, `driverLisPicPath`, `SIN`, `status`) VALUES
(1, 'Shengyong Jiang', 39, '1500', '435 hermitage', 'Pointe Claire', 'QC', 'Canada', 'H9R 4Y2', '514-624-3663', 'shengyongjiang@gmail.com', '2020-05-12 02:35:58', '123456', 'fullTime', 'V123456', 2013, 'DLSh1234', 8, 'Rondo', 'license1.JPG', 120525223, 'on');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `pictureFilePath` varchar(100) NOT NULL,
  `inventory` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `price`, `pictureFilePath`, `inventory`, `categoryId`) VALUES
(1, 'Kafta Arayes', '10.00', 'food1.JPG', 100, 2),
(4, 'Double Chicken Leg\r\n', '16.50', 'food2.JPG', 100, 2),
(5, 'Chicken Wings', '15.00', 'food3.JPG', 100, 2),
(6, 'Cream of chicken', '4.50', 'food4.JPG', 100, 2),
(7, 'Half Chicken', '18.50', 'food5.JPG', 100, 2),
(8, 'Rotisserie Chicken Breast', '15.00', 'food6.JPG', 100, 2),
(9, 'Salade', '15.00', 'food6.JPG', 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(11) NOT NULL,
  `orderNumber` varchar(30) NOT NULL,
  `menuId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`id`, `orderNumber`, `menuId`, `quantity`) VALUES
(3, '2020051106351', 1, 6),
(4, '2020051106351', 4, 1),
(5, '2020051106351', 5, 1),
(6, '2020051106493', 1, 6),
(7, '2020051106493', 4, 1),
(8, '2020051106493', 5, 1),
(9, '2020051106514', 1, 6),
(10, '2020051106514', 4, 1),
(11, '2020051106514', 5, 1),
(12, '2020051205305', 1, 3),
(13, '2020051205305', 4, 1),
(14, '2020051205305', 5, 1),
(15, '2020051205305', 6, 1),
(16, '2020051206316', 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `orderNumber` varchar(30) NOT NULL,
  `customerId` int(11) NOT NULL,
  `deliveryManId` int(11) DEFAULT NULL,
  `orderDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `totalMoney` decimal(10,2) NOT NULL,
  `deliveryType` enum('delivery','pickup') NOT NULL DEFAULT 'delivery',
  `status` enum('pending','confirm','deliveried') NOT NULL DEFAULT 'pending',
  `expectDeliverTime` datetime DEFAULT NULL,
  `deliveryTime` timestamp NULL DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `restaurantId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `orderNumber`, `customerId`, `deliveryManId`, `orderDate`, `totalMoney`, `deliveryType`, `status`, `expectDeliverTime`, `deliveryTime`, `description`, `restaurantId`) VALUES
(2, '2020051106351', 5, 1, '2020-05-12 04:16:53', '42.00', 'delivery', 'deliveried', '2020-05-11 12:20:30', '2020-05-11 16:09:29', NULL, 1),
(3, '2020051106493', 5, 1, '2020-05-12 04:17:05', '42.00', 'delivery', 'deliveried', '2020-05-10 15:25:02', '2020-05-10 19:27:59', NULL, 1),
(4, '2020051106514', 1, 1, '2020-05-12 04:07:53', '42.00', 'delivery', 'confirm', '2020-05-12 11:30:25', NULL, NULL, 1),
(5, '2020051205305', 5, NULL, '2020-05-12 03:46:17', '46.00', 'delivery', 'pending', '2020-05-12 12:00:04', NULL, NULL, 1),
(6, '2020051206316', 1, NULL, '2020-05-12 04:38:05', '15.00', 'delivery', 'pending', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `balance` decimal(10,0) NOT NULL,
  `address` varchar(60) NOT NULL,
  `city` varchar(15) NOT NULL,
  `province` varchar(10) NOT NULL,
  `country` varchar(15) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `email` varchar(255) NOT NULL,
  `registerDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `username` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL,
  `pictureFilePath` varchar(100) NOT NULL,
  `rate` float NOT NULL,
  `openTime` time NOT NULL DEFAULT '08:00:00',
  `closedTime` time NOT NULL DEFAULT '22:00:00',
  `shippingFee` decimal(10,2) NOT NULL DEFAULT 3.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `name`, `owner`, `balance`, `address`, `city`, `province`, `country`, `postcode`, `phone`, `email`, `registerDate`, `username`, `password`, `pictureFilePath`, `rate`, `openTime`, `closedTime`, `shippingFee`) VALUES
(1, 'Moushi', 'Moushi\r\n', '0', '3593 Boul Saint-Jean', 'Pointe Claire', 'QC', 'Canada', 'H9G 1W9', '514-624-3663', 'moushi@gmail.com', '2020-05-03 19:07:59', 'shuixiutan', '123456', 'restaurant1.JPG', 9.5, '00:00:00', '00:00:00', '3.00'),
(2, 'Quesada Burritos & Tacos', 'shuixiutan', '0', '3343 Boulevard des Sources', 'Pointe Claire', 'QC', 'Canada', 'H9B 1Z8', '514-624-3663', 'Quesada@gmail.com', '2020-05-03 19:13:51', 'shuixiutan', '123456', 'restaurant2.JPG', 8.5, '00:00:00', '00:00:00', '0.00'),
(3, 'Toasties', 'Toasties', '0', '4712 Boul Saint-Jean', 'Pointe Claire', 'QC', 'Canada', 'H9H 4B2', '514-624-3663', 'Toasties@gmail.com', '2020-05-03 19:18:05', 'shuixiutan', '123456', 'restaurant3.JPG', 8.3, '00:00:00', '00:00:00', '0.00'),
(4, 'Dairy Queen', 'shuixiutan', '0', '4501 Boulevard Saint-Charles', 'Pointe Claire', 'QC', 'Canada', 'H9H 3C7', '514-624-3663', 'shuixiutan@gmail.com', '2020-05-03 19:20:29', 'shuixiutan', '123456', 'restaurant4.JPG', 7.9, '00:00:00', '00:00:00', '0.00'),
(5, 'Kinton Ramen', 'shuixiutan', '0', '303a Boulevard Brunswick', 'Pointe Claire', 'QC', 'Canada', 'H9R 4Y2', '514-624-3663', 'shuixiutan@gmail.com', '2020-05-03 19:22:16', 'shuixiutan', '123456', 'restaurant5.JPG', 8.8, '00:00:00', '00:00:00', '0.00'),
(6, 'Lahore Karahi Restaurant', 'shuixiutan', '0', '14329 Boul de Pierrefonds', 'Pointe Claire', 'QC', 'Canada', 'H9H 1Z2', '514-624-3663', 'shuixiutan@gmail.com', '2020-05-03 19:24:07', 'shuixiutan', '123456', 'restaurant6.JPG', 9.2, '00:00:00', '00:00:00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(60) NOT NULL,
  `city` varchar(15) NOT NULL,
  `province` varchar(10) CHARACTER SET utf8 NOT NULL,
  `country` varchar(15) NOT NULL DEFAULT 'Canada',
  `postcode` varchar(10) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `email` varchar(255) NOT NULL,
  `registerDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `password` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `city`, `province`, `country`, `postcode`, `phone`, `email`, `registerDate`, `password`) VALUES
(1, 'Maggie Jiang', '5087 walkley', 'Montreal', 'QC', 'Canada', 'H4V2M2', '514-560-0765', 'maggie123@gmail.com', '2020-05-08 01:38:03', '123456'),
(2, 'shuixiutan', '439 hermitage', 'Pointe Claire', 'QC', 'Canada', 'H9R 4Y5', '(438) 345-3986', 'maggie@gmail.com', '2020-05-08 01:43:43', 'Maggie123'),
(5, 'Kevin Jiang', '439 hermitage', 'Pointe Claire', 'QC', 'Canada', 'H4V2M2', '4383453986', 'kevin123@gmail.com', '2020-05-08 02:16:18', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foodItemId` (`menuId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restaurantId` (`restaurantId`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderId` (`orderId`);

--
-- Indexes for table `deliveryemails`
--
ALTER TABLE `deliveryemails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderId` (`orderId`);

--
-- Indexes for table `deliverymen`
--
ALTER TABLE `deliverymen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryId` (`categoryId`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foodItemId` (`menuId`),
  ADD KEY `orderNumber` (`orderNumber`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orderNumber` (`orderNumber`),
  ADD KEY `customerId` (`customerId`),
  ADD KEY `deliverMenId` (`deliveryManId`),
  ADD KEY `restaurantId` (`restaurantId`);

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cartitems`
--
ALTER TABLE `cartitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliveryemails`
--
ALTER TABLE `deliveryemails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliverymen`
--
ALTER TABLE `deliverymen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD CONSTRAINT `cartitems_ibfk_1` FOREIGN KEY (`menuId`) REFERENCES `menus` (`id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Constraints for table `deliveryemails`
--
ALTER TABLE `deliveryemails`
  ADD CONSTRAINT `deliveryemails_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`);

--
-- Constraints for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD CONSTRAINT `orderitems_ibfk_1` FOREIGN KEY (`menuId`) REFERENCES `menus` (`id`),
  ADD CONSTRAINT `orderitems_ibfk_2` FOREIGN KEY (`orderNumber`) REFERENCES `orders` (`orderNumber`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`deliveryManId`) REFERENCES `deliverymen` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
